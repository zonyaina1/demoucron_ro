// import contextMenus from './cytoscape-context-menus';

var cy = cytoscape({
    container: document.getElementById('cy'), // container to render in
  
    style: [ // the stylesheet for the graph
      {
        selector: 'node',
        style: {
          'background-color': '#0D6EFD',
          'text-valign' : "center",
          'text-halign' : "center",
          'label': 'data(label)',
          'width': 75,
          'height': 75
        }
      },
  
      {
        selector: 'edge',
        style: {
          'width': 2,
          'line-color': '#0D6EFD',
          'target-arrow-color': '#0D6EFD',
          'target-arrow-shape': 'triangle',
          'curve-style': 'bezier',
          'label':'data(label)'
        }
      },
      {
        selector : 'node[debut = 1], node[fin=1]',
        style:{
            'background-color' : '#dc3545'
        }
      },
      {
        selector: 'edge[chemin = 1]',
        style: {
          'line-color': '#dc3545',
          'target-arrow-color': '#dc3545',
        }
      },
      {
        selector: 'node[chemin = 1]',
        style: {
          'background-color': '#dc3545',
        }
      }
    ],
  
    layout: {
      name: 'grid',
      rows: 1
    }
  });



cy.on('dbltap', function(evt){
    var valeur = null;
    var valeurAcceptable = false
    while(valeur == null || !valeurAcceptable){
        valeur = prompt("Entrer le nom du sommet: ");
        if(evt.cy.elements('node#'+valeur).length == 0){
            valeurAcceptable = true
        }else{
            valeurAcceptable = false
        }
    }
    var numero = evt.cy.elements('node').length;
    // console.log(numero)
    if(numero == 0){
        evt.cy.add({data : { id : valeur, label : valeur, numero : numero, debut:1}, position : { x : evt.position.x, y : evt.position.y}})
    }else if(numero == 1){
        evt.cy.add({data : { id : valeur, label : valeur, numero : numero, fin : 1}, position : { x : evt.position.x, y : evt.position.y}})
    }else{
        decolorChemin();
        evt.cy.elements('[numero='+(numero-1)+']').data('fin',0)
        // console.log( evt.cy.elements('[numero='+(numero-1)+']').length)
        evt.cy.add({data : { id : valeur, label : valeur, numero : numero, fin : 1}, position : { x : evt.position.x, y : evt.position.y}})
    }
    
    // if(evt.target.data('id') == undefined){
    //     evt.cy.add({data:{id : }})
    // }
});

var options = {
    evtType: 'cxttap',
    menuItems: [
      {
        id: 'remove', // ID of menu item
        content: 'supprimer', // Display content of menu item
        tooltipText: 'supprimer', // Tooltip text for menu item
        selector: 'node, edge', 
        onClickFunction: function (evt) { // The function to be executed on click
          console.log('remove element');
          var target = evt.target || evt.cyTarget;
          if(target.group() != "edges"){
            evt.cy.elements('node[numero='+(target.data('numero')-1)+']').data('fin',1)
          }
        // console.log(evt.cy.elements('[numero='+(target.data('numero')-1)+']').length)
          decolorChemin();
          target.remove();
        }
      },
      {
        id: 'Lier avec',
        content: 'Lier avec',
        tooltipText: 'Lier avec',
        selector: 'node',
        onClickFunction: function () {},
        disabled: false,
        hasTrailingDivider: true,
        // submenuIndicator: { src: 'assets/submenu-indicator-default.svg', width: 12, height: 12 },
      },
      {
        id: 'Changer-de-valeur',
        content: 'Changer de valeur',
        tooltipText: 'Changer de valeur',
        selector: 'edge',
        onClickFunction: function (evt) {
          var nouveauVal=prompt("Entrer nouvelle valeur :");
          if(nouveauVal==null || isNaN(nouveauVal)){nouveauVal=1;}
          evt.target.data('label',nouveauVal);
      },
      disabled: false,
      hasTrailingDivider: true,
      }
    ],
    // css classes that menu items will have
    menuItemClasses: [
      // add class names to this list
    ],
    // css classes that context menu will have
    contextMenuClasses: [
      // add class names to this list
    ],
    // Indicates that the menu item has a submenu. If not provided default one will be used
    submenuIndicator: { src: 'assets/submenu-indicator-default.svg', width: 12, height: 12 }
};

// cy.contextMenus(options)
const instance=cy.contextMenus(options);	

cy.on('cxttap', function(evt){
    var monId=evt.target;
    if(monId.group()=='nodes'){
        instance.removeMenuItem('Lier avec');
        const monMenu={
       id: 'Lier avec',
       content: 'Lier avec',
       tooltipText: 'Lier avec',
       selector: 'node',
       onClickFunction: function () {
         alert('hode element');
       },
       disabled: false,
       hasTrailingDivider: true,
       submenuIndicator: { src: 'assets/submenu-indicator-default.svg', width: 12, height: 12 },
     };
     instance.insertBeforeMenuItem(monMenu,'remove');
      var selectionNode=monId.id();
      var sourceNum = monId.data('numero')

       cy.nodes('node[id !="' + selectionNode+'"]').each(function(node) { 
               var idnode={
         id: node.id(),
         content: node.id(),
         tooltipText: node.id(),
         selector: 'node',
         onClickFunction: function () {
             var valeurArc=prompt("Entrer la valeur de l'arc: ");
            if(valeurArc==null || isNaN(valeurArc)){valeurArc="1";}
             var idEdge=selectionNode+"to"+node.id();
             console.log(idEdge);
           cy.add([{ group: 'edges', data: { id: idEdge, source: selectionNode, sourceNum:sourceNum,label:valeurArc, target: node.id(), targetNum:node.data('numero')} }]);
       },
       disabled: false,
       hasTrailingDivider: true,
       };
       instance.appendMenuItem(idnode,parentID='Lier avec');
        });
    }else if(monId.group()=='edges'){
        console.log("edge")
        console.log(monId)
    }
})

var recuperationMatrice = (valeurParDefaut) => {
    var matrice = []
    if(cy != null){
        var elements = cy.elements();
        var nodes = [];
        var edges = [];
        elements.map(element => {
        if(element.group() == 'nodes'){
            nodes.push(element.data())
        }else if(element.group() == 'edges'){
            edges.push(element.data())
        };
        });
        var tailleMatrice = nodes.length;
        var i, j;
        matrice = creationMatrice(tailleMatrice, valeurParDefaut)
        edges.map(edge =>{
            i = parseInt(edge.sourceNum, 10);
            j = parseInt(edge.targetNum, 10);
            matrice[i][j] = parseInt(edge.label, 10)
        })
    }
    return matrice;
}

var recuperationReference = () => {
    var reference = []
    if(cy != null){
        for(var i=0; i< cy.nodes().length; i++){
            reference.push(cy.nodes('[numero='+i+']').data('label'))
        }
    }
    return reference
}

var trouveMin = async () => {
    decolorChemin()
    var matrice = recuperationMatrice(Infinity)
    let resultat = await algoDemoncronMin(matrice);
    if(resultat.cheminMin === undefined){
        alert("Vérifier le graphe que vous avez entré");
    }else if(resultat.cheminMin === null){
        alert("Impossible de trouver la solution");
    }else{
        colorChemin(resultat.cheminMin);
    }
    var reference = recuperationReference();
    var demarcheDiv = document.getElementById("demarcheDiv");
    var demarcheWrapper = document.getElementById("demarcheWrapper");
    demarcheWrapper.style.display = "block";
        while(demarcheDiv.firstChild){
            demarcheDiv.removeChild(demarcheDiv.firstChild);
        }
        var divEtape = document.createElement('div')
        divEtape.setAttribute('class', 'divEtape')
        demarcheDiv.appendChild(divEtape)
        var table = document.createElement('table');
        table.setAttribute('class','matrice')
        var tr = document.createElement('tr')
        tr.setAttribute('class', 'matriceTopNumero')
        var th = document.createElement('th')
        tr.appendChild(th)
        for(var i = 0; i<reference.length; i++){
            th = document.createElement('th')
            th.innerHTML = reference[i]
            tr.appendChild(th)
        }
        table.appendChild(tr)
        divEtape.appendChild(table)
        matrice.map((ligne,numLigne) => {
            tr = document.createElement('tr');
            var td = document.createElement('td')
            td.setAttribute('class', 'matriceLeftNumero')
            td.innerHTML = reference[numLigne]
            tr.appendChild(td)
            ligne.map(cell => {
                td = document.createElement('td')
                if(cell == Infinity){
                    td.innerHTML = '+&infin;';
                }else{
                    td.innerHTML = cell;
                }
                
                tr.appendChild(td)
            })
            table.appendChild(tr)
        })

        demarches = resultat.demarche
        demarches.map(demarche=>{
            divEtape = document.createElement('div')
            divEtape.setAttribute('class', 'divEtape')
            demarcheDiv.appendChild(divEtape)
            table = document.createElement('table');
            table.setAttribute('class','matrice')
            tr = document.createElement('tr')
            tr.setAttribute('class', 'matriceTopNumero')
            th = document.createElement('th')
            tr.appendChild(th)
            var p = document.createElement('p')
            p.innerHTML = 'K = ' + (demarche.k + 1);
            for(var i = 0; i<reference.length; i++){
                th = document.createElement('th')
                th.innerHTML = reference[i]
                tr.appendChild(th)
            }
            table.appendChild(tr)
            divEtape.appendChild(p)
            divEtape.appendChild(table)
            demarche.currentMatrice.map((ligne,numLigne) => {
                tr = document.createElement('tr');
                var td = document.createElement('td')
                td.setAttribute('class', 'matriceLeftNumero')
                td.innerHTML = reference[numLigne]
                tr.appendChild(td)
                ligne.map(cell => {
                    td = document.createElement('td')
                    if(cell == Infinity){
                        td.innerHTML = '+&infin;';
                    }else{
                        td.innerHTML = cell;
                    }
                    
                    tr.appendChild(td)
                })
                table.appendChild(tr)
            })
        })
    
}

var trouveMax = async () => {
    decolorChemin()
    var matrice = recuperationMatrice(0)
    let resultat = await algoDemoucronMax(matrice);
    if(resultat.cheminMax === undefined){
        alert("Vérifier le graphe que vous avez entré");
    }else if(resultat.cheminMax === null){
        alert("Impossible de trouver la solution");
    }else{
        colorChemin(resultat.cheminMax );
    }

    if(resultat.cheminMax === undefined){
        alert("Vérifier le graphe que vous avez entré");
    }else{
        // colorChemin(resultat.cheminMin);
    }
    var demarcheDiv = document.getElementById("demarcheDiv");
    var reference = recuperationReference()
    var demarcheWrapper = document.getElementById("demarcheWrapper");
    demarcheWrapper.style.display = "block";
    while(demarcheDiv.firstChild){
        demarcheDiv.removeChild(demarcheDiv.firstChild);
    }
        var divEtape = document.createElement('div')
        divEtape.setAttribute('class', 'divEtape')
        demarcheDiv.appendChild(divEtape);
        var table = document.createElement('table');
        table.setAttribute('class','matrice')
        var tr = document.createElement('tr')
        tr.setAttribute('class', 'matriceTopNumero')
        var th = document.createElement('th')
        tr.appendChild(th);
    for(var i = 0; i<reference.length; i++){
        th = document.createElement('th');
        th.innerHTML = reference[i];
        tr.appendChild(th);
    }
    table.appendChild(tr)
    divEtape.appendChild(table)
    matrice.map((ligne,numLigne) => {
        tr = document.createElement('tr');
        var td = document.createElement('td')
        td.setAttribute('class', 'matriceLeftNumero')
        td.innerHTML = reference[numLigne]
        tr.appendChild(td)
        ligne.map(cell => {
            td = document.createElement('td')
            if(cell == Number.NEGATIVE_INFINITY){
                td.innerHTML = '-&infin;';
            }else{
                td.innerHTML = cell;
            }
            
            tr.appendChild(td)
        })
        table.appendChild(tr)
    })

    demarches = resultat.demarche
    demarches.map(demarche=>{
        divEtape = document.createElement('div')
        divEtape.setAttribute('class', 'divEtape')
        demarcheDiv.appendChild(divEtape)
        table = document.createElement('table');
        table.setAttribute('class','matrice')
        tr = document.createElement('tr')
        tr.setAttribute('class', 'matriceTopNumero')
        th = document.createElement('th')
        tr.appendChild(th)
        var p = document.createElement('p')
        p.innerHTML = 'K = ' + (demarche.k + 1);
        for(var i = 0; i<reference.length; i++){
            th = document.createElement('th')
            th.innerHTML = reference[i]
            tr.appendChild(th)
        }
        table.appendChild(tr)
        divEtape.appendChild(p)
        divEtape.appendChild(table)
        demarche.currentMatrice.map((ligne,numLigne) => {
            tr = document.createElement('tr');
            var td = document.createElement('td')
            td.setAttribute('class', 'matriceLeftNumero')
            td.innerHTML = reference[numLigne]
            tr.appendChild(td)
            ligne.map(cell => {
                td = document.createElement('td')
                if(cell == Number.NEGATIVE_INFINITY){
                    td.innerHTML = '-&infin;';
                }else{
                    td.innerHTML = cell;
                }
                
                tr.appendChild(td)
            })
            table.appendChild(tr)
        })
    })
}


var colorChemin = (chemin) => {
    if(cy != null){
      var i;
      for(i = 1; i<chemin.length-1; i++){
        cy.elements('node[numero = '+chemin[i]+']').data('chemin',1);
        // console.log(cy.elements('node[numero = '+chemin[i]+']').length)
        cy.elements('edge[sourceNum = ' +chemin[i]+'][targetNum = '+chemin[i-1]+']').data('chemin',1);
        console.log("eto")
        console.log("color"+chemin[i]+''+chemin[i-1])
        // console.log(chemin[i])
        console.log('test'+cy.elements('edge[sourceNum = ' +chemin[i]+'][targetNum = '+chemin[i-1]+']').length);
    }
        cy.elements('edge[sourceNum = ' +chemin[i]+'][targetNum = '+chemin[i-1]+']').data('chemin',1);
    }
  }
  
  var decolorChemin = () => {
    if(cy != null && cy.elements('[chemin = 1]').length != 0){
      cy.elements('[chemin = 1]').data('chemin', 0);
    }
  }

function creationMatrice(tailleMatrice, valeurParDefaut){
    var matrice = [];
    for(var i=0; i<tailleMatrice; i++){
        matrice[i] = [];
        for(var j=0; j<tailleMatrice; j++){
            matrice[i][j] = valeurParDefaut;
        }
    }
    return matrice;
}

function transposeMat(matrice){
    let tailleMatrice = matrice.length
    let transposedMat = [];
    for(var i=0; i<tailleMatrice; i++){
        transposedMat[i] = new Array(tailleMatrice);
    }
    matrice.forEach((row, i)=>{
        row.forEach((cell, j)=>{
            transposedMat[j][i] = cell;
        })
    })
    return transposedMat;
}

async function copyMatrice(matrice1){
    let matrice = [];
    for(var i=0; i<matrice1.length; i++){
        matrice[i] = [];
        for(var j=0; j<matrice1.length; j++){
            matrice[i][j] = matrice1[i][j];
        }
    }
    return [...matrice];
}


async function algoDemoncronMin(matrice1){
    let demarche = [];
    let matrice = await copyMatrice(matrice1);
    let matriceInitialeTransposed = transposeMat(matrice1);
    // console.log(matrice);
    const tailleMatrice = matrice.length;
    let w = [];
    let entree = [];
    let sortie = [];
    let lastMatrice = [];
    let currentMatrice = [];
    let a_changee = [];
    for(var k=1; k<tailleMatrice-1; k++){
        entree = [];
        sortie = [];
        w=[];
        a_changee = [];
        lastMatrice = await copyMatrice(matrice);
        
        for(var i=0; i<tailleMatrice; i++){
            if(matrice[i][k] !== Infinity){
                entree.push(i)
            }
            if(i == k){
                for(var j=0; j<tailleMatrice; j++){
                    if(matrice[i][j] !== Infinity){
                        sortie.push(j);
                    }
                }
            }
        }
        entree.map((i)=>{
            w[i] = [];
            sortie.map((j)=>{
                w[i][j] = matrice[i][k] + matrice[k][j];
                matrice[i][j] = (w[i][j]<=matrice[i][j] ? w[i][j] : matrice[i][j]);
                a_changee.push([i,j]);
            });
        })
        currentMatrice = await copyMatrice(matrice);
        demarche.push({
            k : k,
            lastMatrice : lastMatrice,
            currentMatrice : currentMatrice,
            a_changee : a_changee,
            entree : entree,
            sortie : sortie,
            w : w
        })
    }
    let cheminMin = undefined;
    if(Number.isFinite(matrice[0][tailleMatrice-1])){
        let matriceMinTransposed= transposeMat(matrice);
        cheminMin = trouveCheminMin(matriceMinTransposed);
    }
    return {
        cheminMin : cheminMin,
        demarche : demarche
    }
}

async function algoDemoucronMax(matrice1){
    let demarche = [];

    let matrice = await copyMatrice(matrice1);
    let matriceInitialeTransposed = transposeMat(matrice1);
    const tailleMatrice = matrice.length;
    let w = [];
    let entree = [];
    let sortie = [];
    let lastMatrice = [];
    let currentMatrice = [];
    let a_changee = [];
    
    for(var k=1; k<tailleMatrice-1; k++){
        entree = [];
        sortie = [];
        w=[];
        a_changee = [];
        lastMatrice = await copyMatrice(matrice);
        for(var i=0; i<tailleMatrice; i++){
            if(matrice[i][k] !== 0){
                entree.push(i)
            }
            if(i == k){
                for(var j=0; j<tailleMatrice; j++){
                    if(matrice[i][j] !== 0){
                        sortie.push(j);
                    }
                }
            }
        }
        entree.map((i)=>{
            w[i] = [];
            sortie.map((j)=>{
                w[i][j] = matrice[i][k] + matrice[k][j];
                matrice[i][j] = (w[i][j]>=matrice[i][j] ? w[i][j] : matrice[i][j]);
                a_changee.push([i,j]);
            });
        });
        currentMatrice = await copyMatrice(matrice);
        demarche.push({
            k : k,
            lastMatrice : lastMatrice,
            currentMatrice : currentMatrice,
            a_changee : a_changee,
            entree : entree,
            sortie : sortie,
            w : w
        })
    }
    let cheminMax = undefined;
    if(Number.isFinite(matrice[0][tailleMatrice-1])){
        let matriceMaxTransposed= transposeMat(matrice);
        cheminMax = trouveCheminMax(matriceMaxTransposed);
    }
    return {
        demarche : demarche,
        cheminMax : cheminMax
    }
}

function indexMin(tableau){
    let min = 0;
    let index = 0
    min = tableau[index];
    tableau.map((val, i)=>{
        if(val<min){
            min = val;
            index = i;
        }
    })
    return index;
}

//cherche le chemin minimun de la matrice final
function trouveCheminMin(matriceTransposed){
    let index = matriceTransposed.length-1;
    let trouve = false;
    let cheminMin = [];
    let indexNext;
    let cumul = 0;
    while(!trouve){
        cheminMin.push(index);
        if(index === 0){
            trouve = true;
        }else{
            indexNext = indexMin(matriceTransposed[index]);
            cumul += matriceTransposed[index][indexNext]
            index = indexNext;
        }
    }
    if(matriceTransposed[matriceTransposed.length-1][0] == cumul){
        return cheminMin;
    }else {
        
        return null;
    }  
}


function indexMax(tableau){
    let max = 0;
    let index = 0;
        max = tableau[index];
    
    tableau.map((val, i)=>{
        if(val != 0){
            if(val<=max){
                max = val;
                index = i;
            }
        }
    })
    return index;
}

function trouveCheminMax(matriceTransposed){
    let index = matriceTransposed.length-1;
    let trouve = false;
    let cheminMax = [];
    let cumul = 0;
    let indexNext;
    while(!trouve){
        cheminMax.push(index);
        if(index === 0){
            trouve = true;
        }else{
            indexNext = indexMax(matriceTransposed[index]);
            cumul += matriceTransposed[index][indexNext];
            index = indexNext;
        }
    }
    if(matriceTransposed[matriceTransposed.length-1][0] == cumul){
        return cheminMax;
    }else {
        console.log(cheminMax)
        console.log(cheminMax)
        return null;
    }  
}